<?php
require_once( __DIR__ . '/../../../vendor/autoload.php');

define('SITE', get_bloginfo('url').'/');
define('TEMA', get_bloginfo('template_url').'/');
define('CSS', get_bloginfo('template_url').'/dist/css/');
define('JS', get_bloginfo('template_url').'/dist/js/');
define('IMG', get_bloginfo('template_url').'/dist/img/');

add_theme_support( 'post-thumbnails');

// add_image_size('slider-home', 1440, 500, true
// addCustonPostType('imovel','Imovel','Imoveis', '', array('title', 'thumbnail'), 'm');
// addCustonTaxonomy('destaque', 'Destaque', 'Destaques', 'destaque', 'm');

add_filter('show_admin_bar', '__return_false');
add_filter('body_class', 'add_body_class');
add_filter('get_archives_link', 'translate_archive_month');

// register carbon fields for all posts
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
  Container::make('theme_options', __( 'Theme Options', 'crb'))
    ->add_fields(array(
        Field::make('text', 'crb_text', 'Text Field'),
    ));
}

add_action('after_setup_theme', 'crb_load');
function crb_load() {
    \Carbon_Fields\Carbon_Fields::boot();
}

add_action('admin_init', 'hide_editor');
add_action('login_enqueue_scripts', 'my_login_logo');
add_action('after_setup_theme', 'register_my_menu');

add_action('wp_enqueue_scripts', 'add_theme_styles');
add_action('wp_footer', 'add_theme_scripts');

// add styles to header
function add_theme_styles()
{
	if (WP_ENV == 'production') {
		wp_enqueue_style('main', CSS.'application.min.css');
	} else {
		wp_enqueue_style('main', CSS.'application.css');
	}
}

// add scripts to footer
function add_theme_scripts()
{
	if (WP_ENV == 'production') {
		wp_enqueue_script('production', JS.'production.min.js');
	} else {
		wp_enqueue_script('livereload', 'http://localhost:35729/livereload.js');
		wp_enqueue_script('production', JS.'production.js');
	}
}

function translate_archive_month($list)
{
  $patterns = array(
    '/January/', '/February/', '/March/', '/April/', '/May/', '/June/', '/July/', '/August/', '/September/', '/October/',  '/November/', '/December/'
  );
  $replacements = array(
    'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
    'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'
  );
  $list = preg_replace($patterns, $replacements, $list);
	return $list;
}

// hide editor in a list of posts
function hide_editor()
{
	// list of post ids
	$ids = array();

  // Get the Post ID.
  if (isset($_GET['post']) || !empty($_POST)) {
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
    if(!isset($post_id)) return;

    // Hide the editor
    if(in_array($post_id, $ids)){
      remove_post_type_support('page', 'editor');
    }
  }
}

// add new custom post type
function addCustonPostType($qry, $singular, $plural, $icon = '',$suporte='',$gen='m')
{
	if ($suporte=='') $suporte = array('title','editor','thumbnail','excerpt','comments');
	if ($gen == 'm') $nov = "novo"; elseif ($gen == 'f') $nov = "nova";
	if ($gen == 'm') $nem = "nenhum"; elseif ($gen == 'f') $nem = "nenhuma";
	$labels = array(
    'name' => _x(ucfirst($plural), 'post type general name'),
    'singular_name' => _x(ucfirst($singular), 'post type singular name'),
    'add_new' => _x(ucfirst($nov) . ' ' . $singular, 'Adicionar '.$singular),
    'add_new_item' => __('Adicionar ' . $nov . ' ' . $singular),
    'edit_item' => __('Editar ' . $singular),
    'new_item' => __(ucfirst($nov) . ' ' . $singular),
    'view_item' => __('Ver '.$singular),
    'search_items' => __('Procurar '.$singular),
    'not_found' =>  __($nem .' '.$singular.' encontrado'),
    'not_found_in_trash' => __($nem . ' ' .$singular.' encontrado no lixo'),
    'parent_item_colon' => '',
    'menu_name' => ucfirst($plural)
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => $suporte
  );
  if($icon) $args['menu_icon'] = $icon;
  register_post_type($qry,$args);
}

// add custom taxonomy
function addCustonTaxonomy($qry, $singular, $plural, $postType = array(),$gen='m')
{
  if ($gen == 'm') $art = "Todos os "; elseif ($gen == 'f') $art = "Todas as ";
  $labels = array(
    'name' => _x( ucfirst($plural), 'taxonomy general name' ),
    'singular_name' => _x( ucfirst($plural), 'taxonomy singular name' ),
    'search_items' =>  __( 'Procurar '.$singular ),
    'all_items' => __( $art.$plural ),
    'parent_item' => __( $singular.' mestre' ),
    'parent_item_colon' => __( 'Parent Genre:' ),
    'edit_item' => __( 'Editar '.$singular ),
    'update_item' => __( 'Atualizar '.$singular ),
    'add_new_item' => __( 'Adicionar '.$singular ),
    'new_item_name' => __( 'Novo nome de '.$singular ),
    'menu_name' => __( ucfirst($plural) )
  );
  $args = array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => $qry ),
  );
  register_taxonomy($qry,$postType, $args);
}

function my_login_logo()
{
  print '
	  <style type="text/css">
	    body.login div#login h1 a {
	      background-image: url('.IMG.'logo_admin.png);
	      padding-bottom: 30px;
	    }
	  </style>';
}

function register_my_menu()
{
  register_nav_menu( 'primary',  'Menu Principal' );
}

// add page slug to body class
function add_body_class($classes)
{
  global $post;
  if (isset($post)) {
    $classes[] = $post->post_type . '-' . $post->post_name;
  }
  return $classes;
}

?>
