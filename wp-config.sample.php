<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/**
 * Sets Enviroment
 */
define('WP_ENV', 'staging');

/**
 * Sets the default theme
 */
define('WP_DEFAULT_THEME', 'default-theme');

/**
 * Sets the wordpress language
 */
define('WPLANG', 'pt_BR');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'uxvNXV)@QdTE|:v~VisA>sK-Aaag#M;X-}zcE*YX_StxJ|*<+C(5@(5 g;N2fa.2');
define('SECURE_AUTH_KEY',  'reJWYIX`,u51B3QEuL&3JfFH.#F)BI7}7*3Q(F3h[[GKkbGpZg7`HgNx+R#mK@fN');
define('LOGGED_IN_KEY',    'rOi3h.Rl+{ Ns6Y3$HwS??pSZ*xaRKBo]nrs9hAI,PX*~-0zG)_Cvp`MKz|{T=3/');
define('NONCE_KEY',        'h qu@0e K:{A B6@ga5+;5cjYdG./P{k9g[|@C&?ko]<kJ;bW(bBJE|F|h?jsD1B');
define('AUTH_SALT',        '1-;`cAc}Tz8hj#-P]vx[PMmXn]6Ri+Y3>?#|EmKrQTV}W =WX)_Js]:<k1:.MMSo');
define('SECURE_AUTH_SALT', '@pT0|0Gzti|<,{!zm-c8EUhK*a`+wC|7-hAO#8W|-!<k]+`~J!8^lDFEW3$6@]G%');
define('LOGGED_IN_SALT',   '3Vy/-)-fTe9<d%yNaabr[hiqZOE:QtlF?en[0YY>-SjAx4&bltR1]x[6F[1IQ L]');
define('NONCE_SALT',       'n7/]IhP(8NWx.`Fs)0&xR^+H4%lgE,Y@z,~H&Y:}Q],C83I-Pve[<;6+aFG80_RY');

/**#@-*/

define('WP_CONTENT_DIR', __DIR__ . '/wp-content');
define('WP_CONTENT_URL', 'http://localhost/default/wp-content');
define('WP_SITEURL', 'http://localhost/default/wp');
define('WP_HOME', 'http://localhost/default');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/* Include composer autoload */
require_once('vendor/autoload.php');
